package fr.miage.filestore.api.resource;

import fr.miage.filestore.api.dto.FileUploadForm;
import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.api.template.Template;
import fr.miage.filestore.api.template.TemplateContent;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.file.*;
import fr.miage.filestore.file.entity.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mp3.Mp3Parser;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.json.JSONArray;
import org.json.JSONObject;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("files")
@OnlyOwner
public class FilesResource {

    private static final Logger LOGGER = Logger.getLogger(FilesResource.class.getName());

    @EJB
    private FileService filestore;

    @EJB
    private AuthenticationService auth;


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response root(@Context UriInfo uriInfo) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files");
        FileItem item = filestore.get("");
        URI root = uriInfo.getRequestUriBuilder().path(item.getId()).build();
        return Response.seeOther(root).build();
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response rootView(@Context UriInfo uriInfo) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files (html)");
        FileItem item = filestore.get("");
        URI root = uriInfo.getRequestUriBuilder().path(item.getId()).path("content").build();
        return Response.seeOther(root).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public FileItem get(@PathParam("id") String id) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files/" + id);
        FileItem item = filestore.get(id);
        return item;
    }

    @GET
    @Path("{id}/content")
    @Template(name = "files")
    @Produces(MediaType.APPLICATION_JSON)
    public Response content(@PathParam("id") String id) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files/" + id + "/content");
        FileItem item = filestore.get(id);
        if ( item.isFolder() ) {
            return Response.ok(filestore.list(item.getId())).build();
        } else {
            return Response.ok(filestore.getContent(item.getId()))
                    .header("Content-Type", item.getMimeType())
                    .header("Content-Length", item.getSize())
                    .header("Content-Disposition", "attachment; filename=" + item.getName()).build();
        }
    }

    @GET
    @Path("{id}/content")
    @Template(name = "files")
    @Produces(MediaType.TEXT_HTML)
    public Response contentView(@PathParam("id") String id, @QueryParam("download") @DefaultValue("false") boolean download) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/files/" + id + "/content (html)");
        FileItem item = filestore.get(id);
        if ( item.isFolder() ) {
            TemplateContent<Map<String, Object>> content = new TemplateContent<>();
            Map<String, Object> value = new HashMap<>();
            value.put("profile", auth.getConnectedProfile());
            value.put("parent", item);
            value.put("path", filestore.path(item.getId()));
            value.put("items", filestore.list(item.getId()));
            content.setContent(value);
            return Response.ok(content).build();
        } else {
            System.out.println(item.getMimeType());
            return Response.ok(filestore.getContent(item.getId()))
                    .header("Content-Type", item.getMimeType())
                    .header("Content-Length", item.getSize())
                    .header("Content-Disposition", ((download) ? "attachment; " : "") + "filename=" + item.getName()).build();
        }
    }

    @GET
    @Path("{id}/thumbnail")
    public Response thumbnail(@PathParam("id") String id) throws FileServiceException, FileItemNotFoundException, IOException {
        FileItem item = filestore.get(id);
        return getThumbnailImg(item);
    }

    @GET
    @Path("{id}/thumbnailMusic")
    public Response thumbnailMusic(@PathParam("id") String id) throws FileServiceException, FileItemNotFoundException, IOException, TikaException, SAXException {
        FileItem item = filestore.get(id);

        //Need Refactor
        if(item.getthumbnailBufferedImage() != null){
            return getThumbnailImg(item);
        }

        InputStream input = filestore.getContent(id);
        ContentHandler handler = new DefaultHandler();
        Metadata metadata = new Metadata();
        Parser parser = new Mp3Parser();
        ParseContext parseCtx = new ParseContext();
        parser.parse(input, handler, metadata, parseCtx);
        input.close();

        String idRelease = getIdRelease(metadata.get("title"), metadata.get("xmpDM:artist"));
        String img = getImg(idRelease);
        if(img != null){
            img = img.replace("http","https");
            BufferedImage bufferedImage = ImageIO.read(new URL(img));
            File f = new File(item.getName());
            ImageIO.write(bufferedImage,"png",f);
            //Buffered image to byteArray.
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage,"png",baos);
            baos.toByteArray();
            //Store the image inside the thumbnail field to avoid calling the API again in the future.
            item.setthumbnailBufferedImage(baos.toByteArray());
            //Need to save updated object in db.
            filestore.update(item);
            return Response.ok(f).build();
        }
        //If there is no picture we return the default image.
        return getImgDefault(item);
    }

    @GET
    @Path("{id}/thumbnailVideo")
    public Response thumbnailVideo(@PathParam("id") String id) throws FileServiceException, FileItemNotFoundException, IOException {
        FileItem item = filestore.get(id);

        if(item.getthumbnailBufferedImage() != null){
            return getThumbnailImg(item);
        }

        String fileNameWithOutExt = FilenameUtils.removeExtension(item.getName());
        String url = "https://imdb-api.com/API/Search/k_1k3yu6wy/" + fileNameWithOutExt;
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(url);
        Response res = target.request().get();
        if(res.getStatus() == 200){
            String value = res.readEntity(String.class);
            JSONObject obj = new JSONObject(value);
            if(!obj.getJSONArray("results").isEmpty()){
                JSONArray arr = obj.getJSONArray("results");
                String image = arr.getJSONObject(0).getString("image");
                client.close();
                res.close();
                URL urlImage = new URL(image);
                BufferedImage bufferedImage = ImageIO.read(urlImage);
                File f = new File(item.getName());
                ImageIO.write(bufferedImage,"png",f);
                //Buffered image to byteArray.
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bufferedImage,"png",baos);
                baos.toByteArray();
                //Store the image inside the thumbnail field to avoid calling the API again in the future.
                item.setthumbnailBufferedImage(baos.toByteArray());
                //Need to save updated object in db.
                filestore.update(item);
                return Response.ok(f).build();
            }
        }
        return getImgDefault(item);
    }


    @POST
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response add(@PathParam("id") String id, @MultipartForm @Valid FileUploadForm form, @Context UriInfo info) throws FileItemAlreadyExistsException, FileServiceException, FileItemNotFoundException {
        LOGGER.log(Level.INFO, "POST /api/files/" + id);
        FileItem item;
        if ( form.getData() != null ) {
            item = filestore.add(id, form.getName(), form.getData());
        } else {
            item = filestore.add(id, form.getName());
        }
        URI createdUri = info.getBaseUriBuilder().path(FilesResource.class).path(item.getId()).build();
        return Response.created(createdUri).build();
    }

    @POST
    @Path("{id}")
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response addView(@PathParam("id") String id, @MultipartForm @Valid FileUploadForm form, @Context UriInfo info) throws FileItemAlreadyExistsException, FileServiceException, FileItemNotFoundException {
        LOGGER.log(Level.INFO, "POST /api/files/" + id + " (html)");
        if ( form.getData() != null ) {
            filestore.add(id, form.getName(), form.getData());
        } else {
            filestore.add(id, form.getName());
        }
        URI createdUri = info.getBaseUriBuilder().path(FilesResource.class).path(id).path("content").build();
        return Response.seeOther(createdUri).build();
    }

    @PUT
    @Path("{id}/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response update(@PathParam("id") String id, @PathParam("name") String name, @MultipartForm FileUploadForm form) throws FileItemNotFoundException, FileServiceException, FileItemNotEmptyException, FileItemAlreadyExistsException {
        LOGGER.log(Level.INFO, "PUT /api/files/" + id + "/" + name);
        filestore.remove(id, name);
        filestore.add(id, name, form.getData());
        return Response.noContent().build();
    }


    @GET
    @Path("{id}/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") String id, @PathParam("name") String name, @Context UriInfo info) throws FileItemNotFoundException, FileServiceException, FileItemNotEmptyException, URISyntaxException {
        LOGGER.log(Level.INFO, "DELETE /api/files/" + name);
        FileItem fileItem = filestore.get(id);
        filestore.remove(fileItem.getParent(), name);
        URI createdUri = info.getBaseUriBuilder().path(FilesResource.class).path(fileItem.getParent()).path("content").build();
        return Response.seeOther(createdUri).build();
        }


    public String getIdRelease(String nameSong, String artist){
        if(nameSong!=null){
            nameSong = nameSong.replaceAll("\\s", "%20");
        }
        else{
            nameSong ="";
        }
        if(artist!= null){
            artist = artist.replace("\\s", "%20");
        }
        else{
            artist="";
        }

        if(nameSong.equals("") && artist.equals("")){ //Si pas de métadonnée
            return null;
        }

        String url = "http://musicbrainz.org/ws/2/release/?query="+nameSong+"%20"+artist+"&fmt=json";
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(url);
        Response res = target.request().get();
        String value = res.readEntity(String.class);
        JSONObject obj = new JSONObject(value);

        JSONArray arr = obj.getJSONArray("releases");
        String idRelease = arr.getJSONObject(0).getString("id");
        client.close();
        res.close();
        return idRelease;
    }

    public String getImg(String idRelease){
        String urlImg = "https://coverartarchive.org/release/" + idRelease;
        Client clientImg = ClientBuilder.newClient();
        WebTarget targetImg = clientImg.target(urlImg);
        Response resImg = targetImg.request().get();
        if(resImg.getStatus() == 307){
            String urlIntermediaire = resImg.getMetadata().get("Location").get(0).toString();
            clientImg.close();
            resImg.close();
            String realUrl = getUrlIntermediaire(urlIntermediaire);
            Client clientReal = ClientBuilder.newClient();
            WebTarget targetReal = clientReal.target(realUrl);
            Response resReal = targetReal.request().get();
            String valueImg = resReal.readEntity(String.class);
            JSONObject obj = new JSONObject(valueImg);
            JSONArray arr = obj.getJSONArray("images");
            String img = arr.getJSONObject(0).getString("image");
            clientReal.close();
            resReal.close();

            return img;
        }
        clientImg.close();
        resImg.close();
        return null;

    }

    public String getUrlIntermediaire(String url){
        Client clientIntermediaire = ClientBuilder.newClient();
        WebTarget targetIntermediaire = clientIntermediaire.target(url);
        Response resIntermediaire = targetIntermediaire.request().get();
        clientIntermediaire.close();
        resIntermediaire.close();
        return resIntermediaire.getMetadata().get("Location").get(0).toString();
    }

    private Response getThumbnailImg(FileItem item) throws IOException {
        if(item.getthumbnailBufferedImage() != null) {
            InputStream is = new ByteArrayInputStream(item.getthumbnailBufferedImage());
            BufferedImage bufferedImage = ImageIO.read(is);
            File f = new File(item.getName());
            ImageIO.write(bufferedImage, "png", f);
            return Response.ok(f).build();
        } else {
            return Response.ok().build();
        }
    }

    private Response getImgDefault(FileItem item) throws IOException {
        URL urlDefault = new URL("https://i.imgur.com/hJkZyrt.png");
        BufferedImage bufferedImage = ImageIO.read(urlDefault);
        File f = new File(item.getName());
        ImageIO.write(bufferedImage, "png", f);
        return Response.ok(f).build();
    }

}

