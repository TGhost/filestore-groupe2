package fr.miage.filestore.thumbnail.pdf;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import fr.miage.filestore.store.binary.BinaryStoreService;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

@Singleton
public class ThumbnailPDFGeneratorBean implements ThumbnailPDFGenerator {

    @EJB
    private BinaryStoreService binaryStoreService;

    @Override
    public byte[] make(String key) {
        try {
            File pdfFile = new File(binaryStoreService.getPath(key));
            RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
            FileChannel channel = raf.getChannel();
            ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
            PDFFile pdf = new PDFFile(buf);
            PDFPage page = pdf.getPage(0);

            // create the image
            Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(),
                    (int) page.getBBox().getHeight());
            BufferedImage bufferedImage = new BufferedImage(rect.width, rect.height,
                    BufferedImage.TYPE_INT_RGB);

            Image image = page.getImage(rect.width, rect.height, rect, null, true, true);
            Graphics2D bufImageGraphics = bufferedImage.createGraphics();
            bufImageGraphics.drawImage(image, 0, 0, null);
            return toByteArray(bufferedImage);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] toByteArray(BufferedImage bi) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, "png", baos);
        return baos.toByteArray();
    }
}
