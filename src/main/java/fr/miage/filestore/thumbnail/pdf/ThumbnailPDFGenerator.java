package fr.miage.filestore.thumbnail.pdf;

public interface ThumbnailPDFGenerator {
    public byte[] make(String key);

}
