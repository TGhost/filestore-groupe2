package fr.miage.filestore.thumbnail.image;

import fr.miage.filestore.store.binary.BinaryStoreService;
import net.coobird.thumbnailator.Thumbnails;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

@Singleton
public class ThumbnailGeneratorBean implements ThumbnailGenerator {

    @EJB
    private BinaryStoreService binaryStoreService;

    @Override
    public byte[] make(String key)  {
        try{
            BufferedImage buff = Thumbnails.of(new File(binaryStoreService.getPath(key)))
                    .size(120,120)
                    .asBufferedImage();
            return toByteArray(buff);

        }catch(Exception e){
            throw new RuntimeException("Error: Couldn't create the thumbnail.");
        }
    }

    private byte[] toByteArray(BufferedImage bi) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi,"png",baos);
        return baos.toByteArray();
    }

}
