package fr.miage.filestore.thumbnail.image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public interface ThumbnailGenerator {
    public byte[] make(String key);

}
